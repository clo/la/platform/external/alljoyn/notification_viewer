package org.alljoyn.ioe.notificationviewer;
/******************************************************************************
* Copyright 2013, Qualcomm Innovation Center, Inc.
*
*    All rights reserved.
*    This file is licensed under the 3-clause BSD license in the NOTICE.txt
*    file for this project. A copy of the 3-clause BSD license is found at:
*
*        http://opensource.org/licenses/BSD-3-Clause. 
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the license is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the license for the specific language governing permissions and
*    limitations under the license.
******************************************************************************/

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

/**
 * This activity shows no UI. It serves the purpose of starting the MPQService by the user. As of Android 3.0,
 * Services cannot be started from BOOT_COMPLETE Intent unless app had previously been explicitly started by the user.
 */
public class DummyActivity extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
        Intent serviceIntent = new Intent(getApplicationContext(), NotificationViewer.class);
        getApplicationContext().startService(serviceIntent);
        finish();
	}
}
